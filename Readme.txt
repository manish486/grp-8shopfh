Group-8 Java-FSD-C4  
Team Leader- Manish Sharma


To Begin with shopforhome group-8 Java-Fsd Project---

(Note: Login credential for normal user is : username: manish@gmail.com, password: mani
(Login credential for Admin is : username: admin@shop.com, password: 1234)

Step 1: Extract the files.

Step 2: Open the discountapi, shopapi, microservice spring boot file in Eclipse software and run every microservice

Step 3: Open the .sql file in MySQL workbench.

Step 4: Open the Angular file in VS Code and install Node Modules.

Step 5: Now run the Spring Boot server.

Step 6: Now run the Angular project ( ng serve -o).

Step 7: Now the website will be running on (http://localhost:4200)

Step 8: Now paste the (localhost:8761) Postman in order to see the data stored in backend

Github URL:  https://github.com/Manish-Shm/gp-8_shopforhome